db.fruits.insertMany([
  {
    name: "Apple",
    color: "Red",
    stock: 20,
    price: 40,
    supplier_id: 1,
    onSale: true,
    origin: ["Philippines", "US"],
  },

  {
    name: "Banana",
    color: "Yellow",
    stock: 15,
    price: 20,
    supplier_id: 2,
    onSale: true,
    origin: ["Philippines", "Ecuador"],
  },

  {
    name: "Kiwi",
    color: "Green",
    stock: 25,
    price: 50,
    supplier_id: 1,
    onSale: true,
    origin: ["US", "China"],
  },

  {
    name: "Mango",
    color: "Yellow",
    stock: 10,
    price: 120,
    supplier_id: 2,
    onSale: false,
    origin: ["Philippines", "India"],
  },
]);

// [SECTION] MongoDB Aggregation
/**
 * MongoDB Aggregation
 * - used to generate manipulated data and perform operations to create filtered results that would help us in analyzing data
 * - Compared to doing CRUD operations, aggregation gives us access to manipulate, filter and compute for results providing us with information to make necessary development decissions.
 */

// Using the AGGREGATE METHOD
/**
 * $match is used to pass the documents that meet the specified condition(s) to the next pipeline stage/aggregation process
 * Syntax:
 *    db.collectionName.aggregate([{$match: {field:value}}])
 */
db.fruits.aggregate([{ $match: { onSale: true } }]);

/**
 * "$group" is used to group elements together and field-value pairs, using the data from grouped elements
 * Syntax:
 *    db.collectionName.aggregate([{$group: {_id: "value", fieldResult:"valueResult"}}])
 */
db.fruits.aggregate([
  { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
]);

/**
 * Aggregating Documents using 2 pipeline stages
 *
 * Syntax:
 *    db.collectionName.aggregate([
 *      {$match: {field: value}},
 *      {$group: {_id: "value", fieldResult:"valueResult"}}
 *    ])
 */
db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
]);

/**
 * Field Projection with aggregation
 *  "$project" can be used the aggregating data to include/exclude fields from the returned results
 * - The field that is set to zero(0) will be excluded
 * - The field that is set to one(1) will be the only one included
 * Syntax:
 *    db.collectionName.aggregate([
 *      {$project:{field: 1/0}}
 *    ])
 */

db.fruits.aggregate([{ $project: { _id: 0 } }]);

db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
  { $project: { _id: 0 } },
]);

/**
 * Sorting aggregated results
 *  $sort - can be used to change the order of the aggregated results
 * - Providing a value of -1 will sort the aggregated results in a Reverse Order
 * Syntax:
 *    db.collectionName.aggregate([
 *      {$sort:{field: 1/-1}}
 *    ])
 *
 * - The field that is set to a value of negative (-1) was sorted descending order
 * - The field that is set to a value of positive (1) was sorted ascending order
 */

db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
  { $sort: { total: -1 } },
]);

// Aggregating results based on the array fields
/**
 * $unwind - deconstructs an array fields from a collection with an array value to give us a result for each array elements.
 * Syntax:
 *    db.collectionName.aggregate([
 *      {$unwind:"$field"}
 *    ])
 */
db.fruits.aggregate([{ $unwind: "$origin" }]);

// Display fruits documents by their origin and the kinds of fruits that are supplied
db.fruits.aggregate([
  { $unwind: "$origin" },
  { $group: { _id: "$origin", kinds: { $sum: 1 } } },
]);
